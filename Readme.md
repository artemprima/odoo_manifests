# Odoo manifests

## create namespace

kubectl apply -f ns.yaml

## create postgresql

kubectl apply -f postgre-service.yaml

kubectl apply -f postgre-statefulset.yaml

## create odoo

1. add image in odoo deployment

2. add ingress in odoo ingress

3. add modules in args

Example:

```
args:
- --init=auth_oauth_keycloak
```
kubectl apply -f . 
